# Exomio Fragmissions

Work diary for Exomio Fragmissions.

The Exomio Fragmissions project modulates global radiophonic spaces with encoded representations of the genetic and conceptual variability of queer/trans/xeno peoples. The research explores questions of genetic determinism, individual differences, and the poetic potentials of blanketing the globe with electromagnetic representations of queer/trans/xeno people. For this first manifestation of the project I explored the digital file formats of genetic data as a form of code poetry, encoded existing research about genetic variations in transgender people into the shortwave mode FT8, and transmitted these signals from Ljubljana, Slovenia, with attempted reception in Taipei, Taiwan, at C-LAB. While not successful in that limited sense, this manifestation did succeed in transmission and the infusion of the local Hertzian space with these representations.

Presented as part of FORKING PIRAGENE in LAB KILL LAB at C-LAB in Taipei, Taiwan.

For more information see [the project website](https://tranxxenolab.net/projects/exomio_fragmissions/).

# Performance video

A streaming video of the performance can be found [here](https//vimeo.com/512952488).

# Streaming Videos

As I was not able to be physically present for LAB KILL LAB, I streamed my work process daily from 9.00h-12.00h CET (16.00h-19.00h CST) from 14.-20.12.2020. These videos, along with other streaming events that were part of my project, can be found on the [C-LAB YouTube channel](https://www.youtube.com/watch?v=d3uLBTa3KcM&list=PLXJ_MjvcL-q7ueohhk4mc4Yo3IBSdp2tP). Note that sometimes there were definite technical issues, especially with sound and bandwidth!

# Credits

Supported by C-LAB.

Special thanks:

Shu Lea Cheang

Ping-Yi Chen

Escher Tsai

Warrick Tsai

Andrew Lin

Dr. Tse-Yi Wang

Imacat

Lea Aymard

